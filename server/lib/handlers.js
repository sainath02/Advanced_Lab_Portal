/*
 * Request Handlers
 *
 */

// * Dependencies
const mysql = require('mysql')
const bcrypt = require('bcrypt')
const db_config = require('./sql')
const _data = require('./data')
const helpers = require('./helpers')

// Mysql db variable create
const mysqlConnection = mysql.createConnection(db_config)

// mysql db connection
mysqlConnection.connect(err => {
	if (!err) console.log('Connection Established Successfully')
	else console.log('Connection Failed!' + JSON.stringify(err, undefined, 2))
})

// Define the Handlers
let handlers = {}

// * User Handler
handlers.users = (data, callback) => {
	const acceptedMethods = ['POST', 'GET', 'PUT', 'DELETE']
	if (acceptedMethods.indexOf(data.method) > -1) {
		handlers._users[data.method](data, callback)
	} else {
		callback(405)
	}
}

// * Container for the user submethods
handlers._users = {}

// Users - POST
// Identifier : type
// Admin : username, password, f_name, l_name, phone, email, gender, role, address
// Optional data : none
handlers._users.POST = (data, callback) => {
	const user = data.payload
	if (user.type == 'admin') {
		query_str = 'insert into admin_table values(?,?,?,?,?,?,?,?,?)'
		bcrypt.hash(data.payload.password, 10, function(hash_err, hash) {
			if (!hash_err) {
				mysqlConnection.query(
					query_str,
					[
						user.username,
						hash,
						user.f_name,
						user.l_name,
						parseInt(user.phone, 10),
						user.email,
						user.gender,
						user.role,
						user.address,
					],
					(err, rows, fields) => {
						if (!err) {
							callback(200)
						} else {
							callback(404, { err })
						}
					}
				)
			} else {
				callback(404, { hash_err })
			}
		})
	} else if (user.type == 'faculty') {
		query_str = 'insert into faculty_table values(NULL,?,?,?,?,?,?,?)'
		bcrypt.hash(data.payload.password, 10, function(hash_err, hash) {
			if (!hash_err) {
				mysqlConnection.query(
					query_str,
					[
						user.f_name,
						user.l_name,
						hash,
						user.gender,
						parseInt(user.phone, 10),
						user.email,
						user.dept,
					],
					(err, rows, fields) => {
						if (!err) {
							callback(200)
						} else {
							callback(404, { err })
						}
					}
				)
			} else {
				callback(404, { hash_err })
			}
		})
	} else if (user.type == 'student') {
		query_str =
			'insert into student_table values(?,?,?,?,?,?,?,?,?,?,?,?,?)'
		bcrypt.hash(data.payload.password, 10, function(hash_err, hash) {
			if (!hash_err) {
				mysqlConnection.query(
					query_str,
					[
						user.rollno,
						hash,
						user.f_name,
						user.l_name,
						user.gender,
						parseInt(user.phone, 10),
						user.email,
						parseInt(user.parent_phone, 10),
						user.parent_email,
						user.dept,
						user.year,
						user.section,
						user.prog,
					],
					(err, rows, fields) => {
						if (!err) {
							callback(200)
						} else {
							callback(404, { err })
						}
					}
				)
			} else {
				callback(404, { hash_err })
			}
		})
	} else {
		callback(404, { err: 'No such user.' })
	}
}

// Users - GET
// Required data : username
// Optional data : none
// TODO Only let an authenticated user access their object. Don't let them access anyone else's
handlers._users.GET = (data, callback) => {
	const user = data.queryStringObject
	if (user.type == 'admin') {
		query_str = 'select * from admin_table where username = ?'
		mysqlConnection.query(query_str, user.username, (err, rows, fields) => {
			if (!err) {
				delete rows[0].password
				callback(200, rows[0])
			} else {
				callback(404, { err })
			}
		})
	} else if (user.type == 'faculty') {
		query_str = 'select * from faculty_table where fid = ?'
		mysqlConnection.query(
			query_str,
			parseInt(user.fid, 10),
			(err, rows, fields) => {
				if (!err) {
					delete rows[0].password
					callback(200, rows[0])
				} else {
					callback(404, { err })
				}
			}
		)
	} else if (user.type == 'student') {
		query_str = 'select * from student_table where rollno = ?'
		mysqlConnection.query(query_str, user.rollno, (err, rows, fields) => {
			if (!err) {
				delete rows[0].password
				callback(200, rows[0])
			} else {
				callback(404, { err })
			}
		})
	} else {
		callback(404, { err: 'No such user.' })
	}
}

// Users - PUT
// Required data : username
// Optional data : firstName, lastName, password, email, parent_phone, parent_email, year, section, programme (at least one must be specified)
// TODO : only let an authenticated use update an object, don't let them update anyone else's
handlers._users.PUT = (data, callback) => {
	// TODO implement update functionality
}

// Users - DELETE
// Required Field : username
// TODO :  Only let authenticated user delete their object. Don't let them delete anyone else's
// TODO : Cleanup any other data files associated with this user.
handlers._users.DELETE = (data, callback) => {
	// TODO : Implement Delete functionality
	const user = data.queryStringObject
	if (user.type == 'admin') {
		query_str = 'delete from admin_table where username = ?'
		mysqlConnection.query(query_str, user.username, (err, rows, fields) => {
			if (!err) {
				callback(200, { Status: 'Admin Deleted' })
			} else {
				callback(404, { err })
			}
		})
	} else if (user.type == 'faculty') {
		query_str = 'delete from faculty_table where fid = ?'
		mysqlConnection.query(
			query_str,
			parseInt(user.fid, 10),
			(err, rows, fields) => {
				if (!err) {
					callback(200, { Status: 'Faculty Deleted' })
				} else {
					callback(404, { err })
				}
			}
		)
	} else if (user.type == 'student') {
		query_str = 'Delete from student_table where rollno = ?'
		mysqlConnection.query(query_str, user.rollno, (err, rows, fields) => {
			if (!err) {
				callback(200, { Status: 'Student Deleted' })
			} else {
				callback(404, { err })
			}
		})
	} else {
		callback(404, { err: 'No such user to delete.' })
	}
}

// * Lab Session Handler
handlers.lab_session = (data, callback) => {
	const acceptedMethods = ['POST', 'GET', 'PUT', 'DELETE']
	if (acceptedMethods.indexOf(data.method) > -1) {
		handlers._lab_session[data.method](data, callback)
	} else {
		callback(405)
	}
}

// * Container for Lab Session sub methods
handlers._lab_session = {}

// Lab session - POST
// TODO : Add relevant info in comments
handlers._lab_session.POST = (data, callback) => {
	const lab = data.payload
	if (lab) {
		query_str =
			"SELECT `AUTO_INCREMENT` FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'session_table'"
		mysqlConnection.query(query_str, (err, rows, fields) => {
			if (!err) {
				sess_id = rows[0].AUTO_INCREMENT
				_data.create(
					'sessions',
					'session_' + sess_id,
					lab.prob_set,
					err => {
						if (!err) {
							query_str =
								'insert into session_table values(NULL,?,?,?,?,?)'
							mysqlConnection.query(
								query_str,
								[
									parseInt(lab.lab_id, 10),
									parseInt(lab.week, 10),
									lab.date,
									'session_' + sess_id,
									'Pending',
								],
								(err, rows, fields) => {
									if (!err) {
										callback(200)
									} else {
										callback(404, {
											err: 'Could not create new Test',
										})
									}
								}
							)
						} else {
							console.log(err)
							callback(500, {
								Error: 'Could not create new Test',
							})
						}
					}
				)
			} else {
				callback(404, { err })
			}
		})
	} else {
		callback(400)
	}
}

// Lab session - GET
// TODO : Add relevant info in comments
handlers._lab_session.GET = (data, callback) => {
	// TODO sending lab_session data
	const lab = data.queryStringObject
	if (lab) {
		query_str = 'select * from session_table where session_id=?'
		mysqlConnection.query(
			query_str,
			lab.session_id,
			(err, rows, fields) => {
				if (!err) {
					console.log(rows[0].prob_set)
					_data.read('sessions', rows[0].prob_set, (err, data) => {
						body = rows[0]
						delete body.prob_set
						console.log(data)
						if (data[0].type == '1') {
							callback(200, {
								statement: data[0].statement,
								test_cases: data[0].test_cases,
							})
						} else if (data[0].type == '2') {
							callback(200, {
								statement: data[0].statement,
								options: data[0].options,
								answer: data[0].answer,
							})
						} else {
							callback(404, { err: 'invalid option' })
						}
					})
				} else {
					callback(404, { err })
				}
			}
		)
	}
}

// Lab session - PUT
// TODO : Add relevant info in comments
handlers._lab_session.PUT = (data, callback) => {
	// TODO : Complete the update lab_session handler i.e, this function
}

// Lab session - DELETE
// TODO : Add relevant info in comments
handlers._lab_session.DELETE = (data, callback) => {
	const lab = data.queryStringObject
	if (lab) {
		query_str = 'select * from session_table where session_id=?'
		mysqlConnection.query(
			query_str,
			lab.session_id,
			(err, rows, fields) => {
				if (!err) {
					console.log(rows[0].prob_set)
					_data.read('sessions', rows[0].prob_set, (err, data) => {
						body = rows[0]
						delete body.prob_set
						console.log(data)
						if (data[0].type == '1') {
							callback(200, {
								statement: data[0].statement,
								test_cases: data[0].test_cases,
							})
						} else if (data[0].type == '2') {
							callback(200, {
								statement: data[0].statement,
								options: data[0].options,
								answer: data[0].answer,
							})
						} else {
							callback(404, { err: 'invalid option' })
						}
					})
				} else {
					callback(404, { err })
				}
			}
		)
	}
}

// handlers._question_type.GET = (data, callback) => {

// }

// * Validate Handler
handlers.lab_session = (data, callback) => {
	const acceptedMethods = ['POST', 'GET', 'PUT', 'DELETE']
	if (acceptedMethods.indexOf(data.method) > -1) {
		handlers._validate[data.method](data, callback)
	} else {
		callback(405)
	}
}

// * Container for Lab Session sub methods
handlers._validate = {}

handlers._validate.POST = (data, callback) => {
	const user = data.payload
	if (user.type == 'admin') {
		query_str = 'select password from admin_table where username = ?'
		bcrypt.compare(user.password, hash, function(err, res) {
			// res == true
		})
	} else if (user.type == 'faculty') {
		query_str = 'insert into faculty_table values(NULL,?,?,?,?,?,?,?)'
		bcrypt.hash(data.payload.password, 10, function(hash_err, hash) {
			if (!hash_err) {
				mysqlConnection.query(
					query_str,
					[
						user.f_name,
						user.l_name,
						hash,
						user.gender,
						parseInt(user.phone, 10),
						user.email,
						user.dept,
					],
					(err, rows, fields) => {
						if (!err) {
							callback(200)
						} else {
							callback(404, { err })
						}
					}
				)
			} else {
				callback(404, { hash_err })
			}
		})
	} else if (user.type == 'student') {
		query_str =
			'insert into student_table values(?,?,?,?,?,?,?,?,?,?,?,?,?)'
		bcrypt.hash(data.payload.password, 10, function(hash_err, hash) {
			if (!hash_err) {
				mysqlConnection.query(
					query_str,
					[
						user.rollno,
						hash,
						user.f_name,
						user.l_name,
						user.gender,
						parseInt(user.phone, 10),
						user.email,
						parseInt(user.parent_phone, 10),
						user.parent_email,
						user.dept,
						user.year,
						user.section,
						user.prog,
					],
					(err, rows, fields) => {
						if (!err) {
							callback(200)
						} else {
							callback(404, { err })
						}
					}
				)
			} else {
				callback(404, { hash_err })
			}
		})
	} else {
		callback(404, { err: 'No such user.' })
	}
}

// * Ping Handler
handlers.ping = (data, callback) => {
	callback(200)
}

// Not Found handler
handlers.notFound = (data, callback) => {
	callback(404)
}

// * Export the module
module.exports = handlers
